
import json
import requests

from .models import AutomobileVO

def get_autos():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")

    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin = auto["vin"],
            defaults={"sold": auto["sold"]},
        )
