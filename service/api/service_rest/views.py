from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVo
# Create your views here.



class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'first_name',
        'last_name',
        'employee_id',
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]
    encoders = {
        'technician': TechnicianEncoder(),
    }

class AutomobileVoEncoder(ModelEncoder):
    model = AutomobileVo
    properties = [
        'vin',
        'sold'
    ]

@require_http_methods(["GET", "POST", "DELETE"])
def technicians_view(request, pk=None):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        new_first_name = content.get("first_name")
        new_last_name = content.get("last_name")
        new_employee_id = content.get("employee_id")

        if new_first_name and new_last_name and new_employee_id:
            Technician.objects.create(
                first_name=new_first_name,
                last_name=new_last_name,
                employee_id=new_employee_id
            )
            return JsonResponse(
                {"message": "Technician created successfully."},
                status=200,
        )
        else:
            return JsonResponse(
                {"error": "Incomplete data provided."},
                status=400,
            )
    else:
        try:
            technician = Technician.objects.get(employee_id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician deleted successfully!"},
                status=200,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technicians not found!"},
                status=400,
            )


@require_http_methods(["GET", "POST", "DELETE"])
def appointments_view(request, vin=None):
    if request.method == "GET":
        if vin:
            try:
                appointment = Appointment.objects.get(vin=vin)
                return JsonResponse(
                    {"appointment": appointment},
                    encoder=AppointmentEncoder,
                )
            except:
                return JsonResponse(
                    {"message": "Appointment not found."},
                    status=404,
                )
        else:
            appointment = Appointment.objects.all()
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentEncoder,
            )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            employeeId = content["technician"]
            technician = Technician.objects.get(employee_id=employeeId)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician ID"},
                status=400,
            )

        new_date_time = content.get('date_time'),
        new_reason = content.get('reason'),
        new_status = content.get('status'),
        new_vin = content.get('vin'),
        new_customer = content.get('customer'),
        new_technician = content.get('technician'),


        if new_date_time and new_reason and new_status and new_vin and new_customer and new_technician:
            new_appointment = Appointment.objects.create(**content)
            return JsonResponse(
                {"message": "Appointment created successfully."},
                status=200,
        )
        else:
            return JsonResponse(
                {"error": "Incomplete data provided."},
                status=400,
            )
    else:
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.delete()
            return JsonResponse(
                {"message": "Appointment deleted successfully!"},
                status=200,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Appointment not found!"})



@require_http_methods(["PUT"])
def appointments_view_cancel(request, vin=None):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.status = "canceled"
            appointment.save()

            return JsonResponse(
                {"message": "Appointment status updated successfully"},
                status=200,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment not found."},
                status=404,
            )
    else:
        return JsonResponse(
            {"error": "Invalid request method."},
            status=400,
        )

@require_http_methods(["PUT"])
def appointments_view_finish(request, vin=None):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(vin=vin)
            appointment.status = "finished"
            appointment.save()

            return JsonResponse(
                {"message": "Appointment status updated successfully"},
                status=200,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment not found."},
                status=404,
            )
    else:
        return JsonResponse(
            {"error": "Invalid request method."},
            status=400,
        )
