import { useEffect, useState } from "react"

function Salespersons() {
    const [salespersons, setSalespersons] = useState([])

    async function getSalespersons() {


        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if (response.ok) {
            const { salespersons } = await response.json();
            setSalespersons(salespersons);
        }
    }

    useEffect(() => {
        getSalespersons();
    }, [])

    return (
        <>
            <h2>Salespersons</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons.map(salesperson => {
                        return(
                            <tr key={salesperson.employee_id}>
                                <td>{ salesperson.employee_id }</td>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default Salespersons
