import './index.css';
import { useState, useEffect } from 'react';

function CreateServiceAppointment() {
    const [newServiceAppointment, setNewServiceAppointment] = useState({
        "date_time": '',
        "reason": '',
        'status': '',
        'vin': '',
        'customer': '',
        'technician': ''
    })

    const [choices, setChoices] = useState([])

    useEffect(() => {
        const apiURL = 'http://localhost:8080/api/technicians/'
        fetch(apiURL)
            .then(response => response.json())
            .then(data => setChoices(data["technicians"]))
            .catch(error => console.error('Error fetching data:', error))
    }, [])

    function handleSubmit(event) {
        // console.log('here')
        // console.log(newServiceAppointment.technician)
        // console.log(newServiceAppointment)
        event.preventDefault();
        const apiURL = "http://localhost:8080/api/appointments/";

        fetch(apiURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newServiceAppointment),
        })
        .then(() => {
            setNewServiceAppointment({
                "date_time": '',
                "reason": '',
                'status': '',
                'vin': '',
                'customer': '',
                'technician': ''
            });
        })
        .catch((error) => console.error('Error creating Technician:', error));
    }

    return (
        <div className="add-service-appointmen">
            <h1>Create a service appointment</h1>
            <form onSubmit={handleSubmit} className='service-appointmen-form'>
                <label>
                    Automobile VIN
                    <input
                        className='service-appointmen-form-input'
                        type="text"
                        value={newServiceAppointment.vin}
                        onChange={(e) => setNewServiceAppointment(
                            {...newServiceAppointment, vin:e.target.value})
                        }
                    />
                </label>
                <label>
                    Customer
                    <input
                        className='service-appointmen-form-input'
                        type="text"
                        value={newServiceAppointment.customer}
                        onChange={(e) => setNewServiceAppointment(
                            {...newServiceAppointment, customer:e.target.value})
                        }
                    />
                </label>
                <label>
                    Date
                    <input
                        className='service-appointmen-form-input'
                        type="datetime-local"
                        value={newServiceAppointment.date_time}
                        onChange={(e) => setNewServiceAppointment(
                            {...newServiceAppointment, date_time:e.target.value})
                        }
                    />
                </label>
                <label>
                    Technician
                </label>
                <select
                    value={newServiceAppointment.technician}
                    onChange={(e) => setNewServiceAppointment({...newServiceAppointment, technician:e.target.value})}

                >
                <option value="">Select Technician</option>
                {
                    choices.map((eachChoice) => {
                        return(
                            <option key={eachChoice.employee_id} value={eachChoice.employee_id}>
                                {`${eachChoice.first_name} ${eachChoice.last_name}`}
                            </option>
                        )
                    })
                }
                </select>
                <label>
                    Reason
                    <input
                        className='service-appointmen-form-input'
                        type="text"
                        value={newServiceAppointment.reason}
                        onChange={(e) => setNewServiceAppointment(
                            {...newServiceAppointment, reason:e.target.value})
                        }
                    />
                </label>
                <button
                    className='service-appointmen-form-create'
                    type="submit">Create</button>
            </form>
        </div>
    )
}


export default CreateServiceAppointment
