import './index.css'
import { useState, useEffect } from 'react';

function Technicians() {
    const [data, setData] = useState([])

    useEffect(() => {
        const apiURL = 'http://localhost:8080/api/technicians/';
        fetch(apiURL)
            .then(response => response.json())
            .then(data => setData(data["technicians"]))
            .catch(error => console.error('Error fetching data:', error))
    }, [])

    const TableRow = ({info}) => {
        return (
            <>
                <tr>
                    <td>{info.first_name}</td>
                    <td>{info.last_name}</td>
                    <td>{info.employee_id}</td>
                    <td className='delete-btn'><button onClick={() => handleDelete(info.employee_id)}>x</button></td>
                </tr>
            </>
        )
    }

    function handleDelete(id) {
        const apiUrl = `http://localhost:8080/api/technicians/${id}`;

        fetch(apiUrl, {
        method: 'DELETE',
        })
        .then((response) => {
            if (!response.ok) {
            throw new Error('Network response was not ok');
            }
            setData((prevData) => prevData.filter((data) => data.employee_id !== id));
        })
        .catch((error) => console.error('Error deleting data:', error));
    };



    return (
        <div className='technicians'>
            <h1>Technicians</h1>
            <table className='technicians-table'>
                <tbody>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>

                    {
                        data.map(info => {
                            return (
                                <TableRow key={info.employee_id} info={info}/>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Technicians
