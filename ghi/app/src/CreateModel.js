import { useState } from "react";
import Manufacturers from "./Manufacturers";

function CreateModel(props) {
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [img, setImg] = useState('')

    function handleManufacturerChange(event) {
        const {value} = event.target;
        setManufacturer(value);
    }

    function handleModelChange(event){
        const {value} = event.target;
        setModel(value);
    }

    function handleUrlChange(event) {
        const {value} = event.target;
        setImg(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name: model,
            manufacturer_id: manufacturer,
            picture_url: img,
        };

        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);

            setImg('')
            setManufacturer('')
            setModel('')
        } else{
            console.log("oh no")
        }
    }

    return(
        <div className="shadow p-4 mt-4">
            <h1>Create Model</h1>
            <form id="model-form" onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select value={manufacturer} onChange={handleManufacturerChange} required name="manufacturer" id="manufacturer" className="form-select">
                        <option value="">Choose a Manufacturer</option>
                        {props.manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.href} value={manufacturer.id}>
                            {manufacturer.name}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={model} onChange={handleModelChange} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
                    <label htmlFor="model">Model</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={img} onChange={handleUrlChange} placeholder="URL" required type="picture_url" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Image URL</label>
                </div>
                <button className="btn btn-primary">Create Model</button>
            </form>
        </div>
    )
}

export default CreateModel
