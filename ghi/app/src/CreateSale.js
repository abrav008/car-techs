import { useEffect, useState } from "react";

function CreateSale() {
    const [ autos, setAutos ] = useState([]);
    const [ salespeople, setSalespersons ] = useState([]);
    const [ customers, setCustomers ] = useState([]);
    const [ vin, setVin ] = useState('');
    const [ salesperson, setSalesperson ] = useState('');
    const [ customer, setCustomer ] = useState('');
    const [ price, setPrice ] =useState('');

    async function getAutos() {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url)

        if (response.ok) {
            const {autos} = await response.json();
            const filteredAutos = autos.filter(auto => auto.sold === false)
            setAutos(filteredAutos);
        }
    }

    async function getSalespersons() {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if (response.ok) {
            const { salespersons } = await response.json();
            setSalespersons(salespersons);
        }
    }

    async function getCustomers() {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)

        if (response.ok) {
            const { customers } = await response.json();
            setCustomers(customers);
        }
    }

    useEffect(() => {
        getAutos();
        getSalespersons();
        getCustomers();
    }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const autoData = {
            sold: true,
        };

        const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify(autoData),
            headers: {
                'Content-Type': 'application/json',
              },
        };

        const autoResponse = await fetch(autoUrl, fetchConfig);
        if (autoResponse.ok) {
            const newData = await autoResponse.json();
            console.log(newData)
        }

        const data = {
            automobile: vin,
            salesperson,
            customer,
            price,
        }

        const url = 'http://localhost:8090/api/sales/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
              },
        };

        const response = await fetch(url, fetchOptions);
        if(response.ok) {
            const newSale = await response.json();
            console.log(newSale)
        }

        setCustomer('')
        setVin('')
        setPrice('')
        setSalesperson('')
        getAutos()
    }

    function handleAutoChange(event) {
        const { value } = event.target;
        setVin(value);
    }

    function handleSalesperonChange(event) {
        const { value } = event.target;
        setSalesperson(value);
    }

    function handleCustomerChange(event) {
        const { value } = event.target;
        setCustomer(value);
    }

    function handlePriceChange(event) {
        const { value } = event.target;
        setPrice(value)
    }

    return(
        <div className="shadow p-4 mt-4">
            <h1>Add a Sale</h1>
            <form id="model-form" onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select value={vin} onChange={handleAutoChange} required name="vin" id="manufacturer" className="form-select">
                        <option value="">Choose a Car</option>
                        {autos.map(auto => {
                        return (
                            <option key={auto.href} value={auto.vin}>
                            {auto.year} {auto.color} {auto.model.name}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={salesperson} onChange={handleSalesperonChange} required name="salesperson" id="salesperon" className="form-select">
                        <option value="">Choose a Salesperson</option>
                        {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                        return (
                            <option key={customer.phone_number} value={customer.phone_number}>
                            {customer.first_name} {customer.last_name}
                            </option>
                        );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" step="0.01" name="price" id="price" className="form-control"  max="100000"/>
                    <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Create Sale</button>
            </form>
        </div>
    )
}

export default CreateSale
