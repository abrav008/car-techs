import { useEffect, useState } from "react"

function Customers() {
    const [customers, setCustomers] = useState([])

    async function getCustomers() {


        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)

        if (response.ok) {
            const { customers } = await response.json();
            setCustomers(customers);
        }
    }

    useEffect(() => {
        getCustomers();
    }, [])

    return (
        <>
            <h2>Customers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return(
                            <tr key={customer.phone_number}>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.phone_number }</td>
                                <td>{ customer.address }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default Customers
