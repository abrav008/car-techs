# CarCar

Team:

* An Tran - Automobile Service
* Anthony Bravo - Automobile Sales

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/anmtrvn/project-beta.git

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/
## Design

CarCar is made up of 3 microservices which interact with one another.

![Alt text](/imgaes/CarCarDiagram.png)
​

## Service microservice
​
The service microservcice has 3 models: Technician, Appointment, and AutomobileVO.
Technician is also a foreign key of Appointment.
AutomobileVO is updated by poller()


```
## Technicians

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technicians | POST | http://localhost:8080/api/technicians/
| Delete a specific technicians | DELETE | http://localhost:8080/api/technicians/:id
​
​
JSON body to send data:
​
Create and Update a technicians (SEND THIS JSON BODY):
- You cannot make two technicians with the same employee_id

Getting a list of technicians. You can also use the same API with employee_id to get specific technician.

{
    "technicians": [
        {
            "first_name": "John",
            "last_name": "Tran",
            "employee_id": 999
        },
        {
            "first_name": "Ken",
            "last_name": "Ken",
            "employee_id": 888
        },
        {
            "first_name": "Jason",
            "last_name": "Woo",
            "employee_id": 98
        }
    ]
}

Create a technicians

{
    "first_name": "Yon",
    "last_name": "Long",
    "employee_id": 123
}

Delete a specific technicians

{
    "first_name": "Jason",
    "last_name": "Woo",
    "employee_id": 98
}


## Appointments

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointments | POST | http://localhost:8080/api/appointments/
| Delete an specific appointments | DELETE | http://localhost:8080/api/appointments/:id
| Get a specific appointments | GET | http://localhost:8080/api/appointments/:id
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel
| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish
​
​
JSON body to send data:
​
Create and Update a technicians (SEND THIS JSON BODY):
You cannot make two technicians with the same employee_id

Getting a list of appointments. You can also use the same API with VIN to get specific appointment.

{
    "appointment": [
        {
            "id": 23,
            "date_time": "2023-07-27T17:22:00+00:00",
            "reason": "Oil Change",
            "status": "",
            "vin": "123456789",
            "customer": "Jack",
            "technician": {
                "first_name": "Ken",
                "last_name": "Ken",
                "employee_id": 888
            }
        },
        {
            "id": 24,
            "date_time": "2023-07-27T17:22:00+00:00",
            "reason": "Tire",
            "status": "",
            "vin": "0987654321",
            "customer": "Barbie",
            "technician": {
                "first_name": "Ken",
                "last_name": "Ken",
                "employee_id": 888
            }
        }
    ]
}

Create an appointments

{
    "id": 24,
    "date_time": "2023-07-27T17:22:00+00:00",
    "reason": "Tire",
    "status": "",
    "vin": "0987654321",
    "customer": "Barbie",
    "technician": {
        "first_name": "Ken",
        "last_name": "Ken",
        "employee_id": 888
    }
}

Delete an specific appointments

{
    "id": 23,
    "date_time": "2023-07-27T17:22:00+00:00",
    "reason": "Oil Change",
    "status": "",
    "vin": "123456789",
    "customer": "Jack",
    "technician": {
        "first_name": "Ken",
        "last_name": "Ken",
        "employee_id": 888
    }
}

Get a specific appointments

{
    "id": 24,
    "date_time": "2023-07-27T17:22:00+00:00",
    "reason": "Tire",
    "status": "",
    "vin": "0987654321",
    "customer": "Barbie",
    "technician": {
        "first_name": "Ken",
        "last_name": "Ken",
        "employee_id": 888
    }
}


Set appointment status to "canceled"

{
            "id": 23,
            "date_time": "2023-07-27T17:22:00+00:00",
            "reason": "Oil Change",
            "status": "canceled",
            "vin": "123456789",
            "customer": "Jack",
            "technician": {
                "first_name": "Ken",
                "last_name": "Ken",
                "employee_id": 888
            }
        }

Set appointment status to "finished"

{
            "id": 24,
            "date_time": "2023-07-27T17:22:00+00:00",
            "reason": "Tire",
            "status": "finished",
            "vin": "0987654321",
            "customer": "Barbie",
            "technician": {
                "first_name": "Ken",
                "last_name": "Ken",
                "employee_id": 888
            }
        }
```

## Sales microservice

The sales microservcice has 4 models: Customer, Salesperson/Employee, Sales, and AutomobileVO. Sales gets data from the other three models and AutomboilVO gets data via the poller.

    ### Customers:


        | Action | Method | URL
        | ----------- | ----------- | ----------- |
        | List customers | GET | http://localhost:8090/api/customers/
        | Create a customer | POST | http://localhost:8090/api/customers/
        | Delete a customer | DELETE | http://localhost:8090/api/customers/id/

    To create a Customer (SEND THIS JSON BODY):
    ```
    {
        "first_name": "Buyer",
        "last_name": "Mcbuys",
        "address": "209 Winchester Ave",
        "phone_number": "3231234567"
    }
    ```
    Return Value of Creating a Customer:
    ```
    {
        "first_name": "Buyer",
        "last_name": "Mcbuys",
        "address": "209 Winchester Ave",
        "phone_number": "3231234567",
        "id": 1
    }
    ```
    Return value of Listing all Customers:
    ```
    {
        "customers": [
            {
                "first_name": "Buyer",
                "last_name": "Mcbuys",
                "address": "209 Winchester Ave",
                "phone_number": 3231234567,
                "id": 1
            },
            {
                "first_name": "Shady",
                "last_name": "Shades",
                "address": "3454 Shady ln, shady, sh",
                "phone_number": 3230000000,
                "id": 2
            },
            {
                "first_name": "Poor",
                "last_name": "Guy",
                "address": "4000 Poor Guy ln",
                "phone_number": 555,
                "id": 3
            },
            {
                "first_name": "SLow",
                "last_name": "fas",
                "address": "4000 Poor Guy ln",
                "phone_number": 7858973489,
                "id": 4
            }
        ]
    }
    ```

    ### Salespeople:
    | Action | Method | URL
    | ----------- | ----------- | ----------- |
    | List salespeople | GET | http://localhost:8090/api/salespeople/
    | Create a salesperson | POST | http://localhost:8090/api/salespeople/
    | Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/

    To create a salesperson (SEND THIS JSON BODY):
    ```
    {
        "first_name": "Sales",
        "last_name": "Mcseller",
        "employee_id": "12345678"
    }
    ```
    Return Value of creating a salesperson:
    ```
    {
        "first_name": "Sales",
        "last_name": "Mcseller",
        "employee_id": "12345678",
        "id": 1
    }
    ```
    List all salespeople Return Value:
    ```
    {
        "salespersons": [
            {
                "first_name": "Sales",
                "last_name": "Mcseller",
                "employee_id": "12345678",
                "id": 1
            },
            {
                "first_name": "Work",
                "last_name": "Hard",
                "employee_id": "Whard",
                "id": 2
            },
            {
                "first_name": "Hard",
                "last_name": "Worker",
                "employee_id": "HWorker",
                "id": 3
            },
            {
                "first_name": "Never",
                "last_name": "Works",
                "employee_id": "NWorks",
                "id": 4
            },
            {
                "first_name": "Always",
                "last_name": "Grdinging",
                "employee_id": "AGrind",
                "id": 5
            },
            {
                "first_name": "Light",
                "last_name": "Bravo",
                "employee_id": "LBravo",
                "id": 6
            }
        ]
    }
    ```

```
    ### Sales:

    | Action | Method | URL
    | ----------- | ----------- | ----------- |
    | List all sales| GET | http://localhost:8090/api/sales/
    | Create a new sale | POST | http://localhost:8090/api/sales/
    | Delete sale| DELETE | http://localhost:8090/api/sales/id/

    List all SalesReturn Value:
    ```
    {
        "sales": [
            {
                "price": 23412.234,
                "id": 3,
                "automobile": {
                    "vin": "1C3CC5FB2AN120175",
                    "sold": true,
                    "id": 2
                },
                "salesperson": {
                    "first_name": "Sales",
                    "last_name": "Mcseller",
                    "employee_id": "12345678",
                    "id": 1
                },
                "customer": {
                    "first_name": "Buyer",
                    "last_name": "Mcbuys",
                    "address": "209 Winchester Ave",
                    "phone_number": 3231234567,
                    "id": 1
                }
            }
        ]
    }
    ```
    Create a New Sale (SEND THIS JSON BODY):
    ```
    {
        "automobile": "1C3CC5FB2AN120189",
        "salesperson": "12345678",
        "customer": "3231234567",
        "price": "25000"
    }
    ```
    Return Value of Creating a New Sale:
    ```
    {
        "price": "25000",
        "id": 2,
        "automobile": {
            "vin": "1C3CC5FB2AN120189",
            "sold": false,
            "id": 3
        },
        "salesperson": {
            "first_name": "Sales",
            "last_name": "Mcseller",
            "employee_id": "12345678",
            "id": 1
        },
        "customer": {
            "first_name": "Buyer",
            "last_name": "Mcbuys",
            "address": "209 Winchester Ave",
            "phone_number": 3231234567,
            "id": 1
        }
    }
    ```
